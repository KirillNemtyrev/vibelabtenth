package org.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int year = Integer.parseInt(scanner.nextLine().trim().substring(6));

        System.out.println(((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0) ? "True" : "False");
    }
}